<?php
include ('connect.php');
include ('includes/connection.php');
date_default_timezone_set('Europe/Kiev');
$array = array('ESETESI.zip', 'ESETESII.zip','ESETESIII.zip', 'ESETESIV.zip', 'ESETESV.zip');  // упрощеный вид скрипта оброботчика, анализа все файлов для скачивания с проверкой тут
if ($_GET['download'] == $array[0] ||  $_GET['download'] == $array[1] || $_GET['download'] == $array[2] || $_GET['download'] == $array[3] || $_GET['download'] == $array[4]) {
    ignore_user_abort(1);
    $file = strip_tags($_GET['download']); // убераю html теги
    $file = htmlspecialchars($file); //преобразует спец. символы в html сущности. Так вы защитите себя от XSS атаки, помимо SQL инъекции.
    if (!is_readable($file)) {
        die('No such file');
    }
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . basename($file));
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: max-age=0; must-revalidate');
    header('Content-Length: ' . filesize($file));


    $count = 0;

    $ih = fopen($file, 'r');
    while (CONNECTION_NORMAL == connection_status() && !feof($ih)) {
        print fgets($ih, 4096);
        if (feof($ih)) {

            $connect = connectPDO();
            $sql = "UPDATE `Downloads` SET `$file`  =  `$file` + 1 ";
            $result = $connect->query($sql);
            $statistic = R::dispense("statistic");
            $statistic->login = $_SESSION["login"];
            $statistic->name_file = $file;
            $statistic->time = date(DATE_RSS);
            $statistic->result = "Download compile";
            R::store($statistic);
            $connect = null;

        }
    }

    if (!feof($ih)) {
        $statistic = R::dispense("statistic");
        $statistic->login = $_SESSION["login"];
        $statistic->name_file = $file;
        $statistic->time = date(DATE_RSS);
        $statistic->result = "Download no compile";
        R::store($statistic);
    }
} else {
    echo "ты плохой человек который пытаеться нас взламать я тебя найду по ip";
    echo $_SESSION["login"];
}
?>