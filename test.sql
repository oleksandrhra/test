-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 13 2017 г., 15:21
-- Версия сервера: 5.7.19
-- Версия PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Downloads`
--

CREATE TABLE `Downloads` (
  `id` int(7) NOT NULL,
  `esetesi.zip` int(7) NOT NULL,
  `esetesii.zip` int(7) NOT NULL,
  `esetesiii.zip` int(7) NOT NULL,
  `esetesiv.zip` int(7) NOT NULL,
  `esetesv.zip` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Downloads`
--

INSERT INTO `Downloads` (`id`, `esetesi.zip`, `esetesii.zip`, `esetesiii.zip`, `esetesiv.zip`, `esetesv.zip`) VALUES
(1, 9, 1, 2, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `statistic`
--

CREATE TABLE `statistic` (
  `id` int(11) UNSIGNED NOT NULL,
  `login` tinyint(1) UNSIGNED DEFAULT NULL,
  `name_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `result` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `statistic`
--

INSERT INTO `statistic` (`id`, `login`, `name_file`, `time`, `result`) VALUES
(1, NULL, NULL, 'Wed, 13 Dec 2017 14:14:55 +0200', 'Не скачав'),
(2, NULL, NULL, 'Wed, 13 Dec 2017 14:14:56 +0200', 'Не скачав'),
(3, NULL, NULL, 'Wed, 13 Dec 2017 14:15:02 +0200', 'Не скачав'),
(4, NULL, 'ESETESIV.zip', 'Wed, 13 Dec 2017 14:18:54 +0200', 'Скачав');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `law` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reg_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `law`, `reg_time`) VALUES
(1, 'Besame', '9ee0ca3e24d60ea0b6a3e02892877bc0e2324d377e87eca76be1176d73c574e4', 'user', 'Wed, 13 Dec 2017 14:01:20 +0200'),
(2, 'Besame', '9ee0ca3e24d60ea0b6a3e02892877bc0e2324d377e87eca76be1176d73c574e4', 'user', 'Wed, 13 Dec 2017 14:01:37 +0200'),
(3, 'Besame', '6bdaf554e42029bd0dbee9471eb48a5f3626213c9859a58120a61c62ace4c968', 'user', 'Wed, 13 Dec 2017 14:18:11 +0200'),
(4, 'III', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 'user', 'Wed, 13 Dec 2017 14:18:40 +0200');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Downloads`
--
ALTER TABLE `Downloads`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `statistic`
--
ALTER TABLE `statistic`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Downloads`
--
ALTER TABLE `Downloads`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `statistic`
--
ALTER TABLE `statistic`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
